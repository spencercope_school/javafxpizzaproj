//Spencer Cope cs2261-E01
package javafxPizzaProj;

import java.text.DecimalFormat;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;

public class MainActivity extends Application {
    private static DecimalFormat df2 = new DecimalFormat("0.00");
    Button lion, lionDefault;
    Scene scene1, scene2;
    ImageView cheeseImgView, veggieImg, pepImg;
    double cPrice = 1.5;
    double vPrice = 2;
    double pPrice = 2.25;
    double sum = 0.00;
    Label totalText =  new Label("Total: $" + df2.format(sum));
    int numCheeseSlices, numPepSlices, numVeggieSlices;
    double outSum;
    //file:D:\\Desktop\\cheese1.jp
    
    public static void main(String[] args){
        launch(args);
    }
    
    @Override
    public void start(Stage stage) {     
        stage.setTitle("The Pizza Project");

        Image cheeseImg = new Image("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQsPUYJCE1GdNVz0ebYS4yg9d8MkU49PYbDnCWY3g4q4BsaEj7Y",200,200,false,false);
        ImageView cheeseView = new ImageView(cheeseImg);
        cheeseView.setX(100);
        cheeseView.setY(100);
        Image pepImg = new Image("http://mykcpizza.com/wp-content/uploads/2015/10/PEPPERONI.png",200,200,false,false);
        ImageView pepView = new ImageView(pepImg);
        pepView.setX(100);
        pepView.setY(100);
        Image veggieImg = new Image("http://comps.canstockphoto.com/can-stock-photo_csp16023425.jpg",200,200,false,false);
        ImageView veggieView = new ImageView(veggieImg);
        veggieView.setX(100);
        veggieView.setY(100);
        
        Arc arc = new Arc();
        arc.setCenterX(50.0f);
        arc.setCenterY(50.0f);
        arc.setRadiusX(25.0f);
        arc.setRadiusY(25.0f);
        arc.setStartAngle(45.0f);
        arc.setLength(270.0f);
        arc.setType(ArcType.ROUND);
        
        HBox centerTopHBox = new HBox();
        centerTopHBox.setPadding(new Insets(10, 10, 10, 10));
        centerTopHBox.setSpacing(10);
        centerTopHBox.getChildren().addAll(pepView);
        
        
        Button submitBtn = new Button("Place Order");
        submitBtn.getStyleClass().add("lion-default");
        Button resetBtn = new Button("Reset");
        submitBtn.getStyleClass().add("ipad-grey");
        resetBtn.setVisible(false);
        submitBtn.setDisable(true);
        
        Label thanksLabel = new Label("Thank you for your purchase!");
        thanksLabel.setVisible(false);
        
        HBox submitBox = new HBox();
        submitBox.setPadding(new Insets(10, 10, 10, 10));
        submitBox.setSpacing(10);
        submitBox.getChildren().addAll(submitBtn);

        HBox centerBottomHBox = new HBox();
        centerBottomHBox.setPadding(new Insets(10, 10, 10, 10));
        centerBottomHBox.setSpacing(10);
        centerBottomHBox.getChildren().addAll(resetBtn, thanksLabel);
        
        HBox centerMidHBox = new HBox();
        centerMidHBox.setPadding(new Insets(10, 10, 10, 10));
        centerMidHBox.setSpacing(10);
        centerMidHBox.getChildren().addAll(cheeseView, veggieView);
        
        Label leftVBoxTopLabel = new Label("Pick your pizza");
        Label cheeseLabel = new Label("Cheese\n($1.50 per slice)");
        Label veggieLabel = new Label("Veggie\n($2.00 per slice)");
        Label pepLabel = new Label("Pepperoni\n($2.25 per slice)");
        
        
        ObservableList<String> cheeseNumChoices = FXCollections.observableArrayList(
            "0","1","2","3","4","5","6","7","8"
         );
        final ComboBox cheeseNumComboBox = new ComboBox(cheeseNumChoices);
        cheeseNumComboBox.getSelectionModel().selectFirst();
        
        ObservableList<String> veggieNumChoices = FXCollections.observableArrayList(
            "0","1","2","3","4","5","6","7","8"
         );
        final ComboBox veggieNumComboBox = new ComboBox(veggieNumChoices);
        veggieNumComboBox.getSelectionModel().selectFirst();
        
        ObservableList<String> pepNumChoices = FXCollections.observableArrayList(
            "0","1","2","3","4","5","6","7","8"
         );
        final ComboBox pepNumComboBox = new ComboBox(pepNumChoices);
        pepNumComboBox.getSelectionModel().selectFirst();
        
        HBox totalLabelBox = new HBox();
        totalLabelBox.setPadding(new Insets(10, 10, 10, 10));
        totalLabelBox.setSpacing(10);
        totalLabelBox.getChildren().addAll(totalText);
        
        HBox cheeseBox = new HBox();
        cheeseBox.setPadding(new Insets(10, 10, 10, 10));
        cheeseBox.setSpacing(10);
        cheeseBox.getChildren().addAll(cheeseLabel, cheeseNumComboBox);
        
        HBox veggieBox = new HBox();
        veggieBox.setPadding(new Insets(10, 10, 10, 10));
        veggieBox.setSpacing(10);
        veggieBox.getChildren().addAll(veggieLabel, veggieNumComboBox);
        
        HBox pepBox = new HBox();
        pepBox.setPadding(new Insets(10, 10, 10, 10));
        pepBox.setSpacing(10);
        pepBox.getChildren().addAll(pepLabel, pepNumComboBox);
        
        Button calcBtn = new Button("Calculate");
        calcBtn.getStyleClass().add("lion");
        HBox calcBox = new HBox();
        calcBox.setPadding(new Insets(10, 10, 10, 10));
        calcBox.setSpacing(10);
        calcBox.getChildren().addAll(calcBtn);
        calcBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                numCheeseSlices = Integer.parseInt((String) cheeseNumComboBox.getValue());
                numPepSlices = Integer.parseInt((String) pepNumComboBox.getValue());
                numVeggieSlices = Integer.parseInt((String) veggieNumComboBox.getValue());
                sum = (vPrice * numVeggieSlices) + (pPrice * numPepSlices) + (cPrice * numCheeseSlices);
                totalText.setText("Total: $" + df2.format(sum));
                submitBtn.setDisable(false);
            }
        });
        
        submitBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                resetBtn.setVisible(true);
                thanksLabel.setVisible(true);
            }
        });
        
        resetBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                cheeseNumComboBox.getSelectionModel().selectFirst();
                pepNumComboBox.getSelectionModel().selectFirst();
                veggieNumComboBox.getSelectionModel().selectFirst();
                submitBtn.setDisable(true);
                resetBtn.setVisible(false);
                thanksLabel.setVisible(false);
                sum = 0.0;
                totalText.setText("Total: $" + df2.format(sum));
            }
        });

        VBox leftVBox = new VBox();
        leftVBox.setPadding(new Insets(10, 10, 10, 10));
        leftVBox.setSpacing(10);
        leftVBox.getChildren().addAll(leftVBoxTopLabel, cheeseBox, veggieBox, pepBox, totalLabelBox, calcBox, submitBox);
        
        VBox centerVBox = new VBox();
        centerVBox.setPadding(new Insets(10, 10, 10, 10));
        centerVBox.setSpacing(10);
        centerVBox.getChildren().addAll(centerTopHBox, centerMidHBox, centerBottomHBox, arc);

        BorderPane bp = new BorderPane();
        bp.setLeft(leftVBox);
        bp.setCenter(centerVBox);
        
        scene1 = new Scene(bp, 900, 600);
        scene1.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        stage.setScene(scene1);
        stage.show();
        //AlertBox.display("Welcome!","\n    initial alertBox for whatever");
       
        
    }

    
}
